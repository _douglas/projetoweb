# README #

This is the project for class PCO002.

### UML Model ###

![modelo.png](https://bitbucket.org/repo/5E8XgX/images/3248422438-modelo.png)

### Used technologies ###

* JDK 8
* MAVEN
* JUnit
* Selenium
* JSF Mojarra
* JSF Primefaces
* Design Patterns
* MVC
* PostgreSQL (can be changed on pom.xml)
* Jboss or Tomcat

### Project Management ###

* Git Flow
* UML
* Jenkins
* Docker


### Needs on environment ###

* Eclipse Mars (can use another)
* JDK 8
* JBoss or Tomcat