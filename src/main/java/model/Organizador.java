package model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Organizador extends Pessoa implements IOrganizador {

	private double horasDedicadas;

	@ManyToOne
	private Organizadora organizadora = new Organizadora();

	public double getHorasDedicadas() {
		return horasDedicadas;
	}

	public void setHorasDedicadas(double horasDedicadas) {
		this.horasDedicadas = horasDedicadas;
	}

	public Organizadora getOrganizadora() {
		return organizadora;
	}

	public void setOrganizadora(Organizadora organizadora) {
		this.organizadora = organizadora;
	}

}
