package model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class HorarioReserva {

	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	private Date dia;
	private int horaEntrada;
	private int minEntrada;
	private int horaSaida;
	private int minSaida;
	
	public Date getDia() {
		return dia;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	public int getHoraEntrada() {
		return horaEntrada;
	}
	public void setHoraEntrada(int horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	public int getMinEntrada() {
		return minEntrada;
	}
	public void setMinEntrada(int minEntrada) {
		this.minEntrada = minEntrada;
	}
	public int getHoraSaida() {
		return horaSaida;
	}
	public void setHoraSaida(int horaSaida) {
		this.horaSaida = horaSaida;
	}
	public int getMinSaida() {
		return minSaida;
	}
	public void setMinSaida(int minSaida) {
		this.minSaida = minSaida;
	}
	
	
}
