package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Evento {
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String nome;
	private double verbaDispoonivel;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Esportista> esportistas = new ArrayList<Esportista>();
	@OneToMany(cascade=CascadeType.ALL)
	private List<Patrocinador> patrocinadores = new ArrayList<Patrocinador>();
	@OneToMany(cascade=CascadeType.ALL)
	private List<Organizador> organizadores = new ArrayList<Organizador>();
	
	@OneToOne(cascade=CascadeType.ALL)
	private Categoria categoria = new Categoria();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getVerbaDispoonivel() {
		return verbaDispoonivel;
	}

	public void setVerbaDispoonivel(double verbaDispoonivel) {
		this.verbaDispoonivel = verbaDispoonivel;
	}

	public List<Esportista> getEsportistas() {
		return esportistas;
	}

	public void setEsportistas(List<Esportista> esportistas) {
		this.esportistas = esportistas;
	}

	public List<Patrocinador> getPatrocinadores() {
		return patrocinadores;
	}

	public void setPatrocinadores(List<Patrocinador> patrocinadores) {
		this.patrocinadores = patrocinadores;
	}

	public List<Organizador> getOrganizadores() {
		return organizadores;
	}

	public void setOrganizadores(List<Organizador> organizadores) {
		this.organizadores = organizadores;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	
}
