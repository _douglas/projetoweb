package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Local {
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	private String nome;
	
	private String bairro;
	private String rua;
	private String numero;
	private String cep;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<HorarioReserva> horariosAlugados = new ArrayList<HorarioReserva>();
	
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public List<HorarioReserva> getHorariosAlugados() {
		return horariosAlugados;
	}
	public void setHorariosAlugados(List<HorarioReserva> horariosAlugados) {
		this.horariosAlugados = horariosAlugados;
	}
	public abstract double getValorAluguel();
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	
	
}
