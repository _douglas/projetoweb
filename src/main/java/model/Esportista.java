package model;

import javax.persistence.Entity;

@Entity
public class Esportista extends Pessoa {

	private String esporte;

	public String getEsporte() {
		return esporte;
	}

	public void setEsporte(String esporte) {
		this.esporte = esporte;
	}
	
}
