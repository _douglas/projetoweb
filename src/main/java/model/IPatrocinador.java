package model;

public interface IPatrocinador {
	
	public double getVerbaAInvestir();
	public void setVerbaAInvestir(double verbaAInvestir);
	public double patrocina(double dinheiroADoar);
}
