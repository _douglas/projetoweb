package model;

import javax.persistence.Entity;

@Entity
public class Patrocinadora extends Empresa implements IPatrocinador {

	private double verbaAInvestir;


	public double getVerbaAInvestir() {
		return verbaAInvestir;
	}

	public void setVerbaAInvestir(double verbaAInvestir) {
		this.verbaAInvestir = verbaAInvestir;
	}

	public double patrocina(double dinheiroADoar) {
		if (verbaAInvestir < dinheiroADoar)
			return -1;
		else
			return verbaAInvestir -= dinheiroADoar;
	}
	
	public String getTipo(){
		return getClass().getSimpleName();
	}

}
