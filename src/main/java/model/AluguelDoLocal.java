package model;

import java.util.Calendar;

public class AluguelDoLocal {

	public AluguelDoLocal (Local local, Evento evento, HorarioReserva horario) {
		if(!local.getHorariosAlugados().contains(horario)){
			if(evento.getVerbaDispoonivel() >= local.getValorAluguel()){
				local.getHorariosAlugados().add(horario);
				evento.setVerbaDispoonivel(evento.getVerbaDispoonivel() - local.getValorAluguel());
			}
		}
	}

}
