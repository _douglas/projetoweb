package model;

import javax.persistence.Entity;

@Entity
public class LocalPrivado extends Local {

	private double valorAluguel;

	public double getValorAluguel() {
		return valorAluguel;
	}

	public void setValorAluguel(double valorAluguel) {
		this.valorAluguel = valorAluguel;
	}
	
	
	
}
