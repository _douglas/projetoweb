package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Organizadora extends Empresa implements IOrganizador {

	@OneToMany
	private List<Organizador> organizadores = new ArrayList<Organizador>();

	private double horasDedicadasDostrabalhadores = 0;

	public String getTipo() {
		return getClass().getSimpleName();
	}

	public double getHorasDedicadas() {
		return horasDedicadasDostrabalhadores;
	}

	public void setHorasDedicadasDostrabalhadores() {
		if (organizadores.isEmpty())
			horasDedicadasDostrabalhadores = 0;
		else{
			for(Organizador o : organizadores){
				horasDedicadasDostrabalhadores += o.getHorasDedicadas();
			}
		}
	}

	public List<Organizador> getOrganizadores() {
		return organizadores;
	}

	public void setOrganizadores(List<Organizador> organizadores) {
		this.organizadores = organizadores;
	}

	public double getHorasDedicadasDostrabalhadores() {
		return horasDedicadasDostrabalhadores;
	}

	public void setHorasDedicadasDostrabalhadores(double horasDedicadasDostrabalhadores) {
		this.horasDedicadasDostrabalhadores = horasDedicadasDostrabalhadores;
	}
	
	

}
