package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;

import dao.DAO;
import dao.JPAUtil;
import dao.PessoaDao;
import model.Pessoa;

@ManagedBean
@ApplicationScoped
public class PessoaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Pessoa pessoa = new Pessoa();
	public List<Pessoa> pessoas = new ArrayList<Pessoa>();
	public List<Pessoa> pessoasEncontradas = new ArrayList<Pessoa>();

	public PessoaBean() {
		this.findAll();
	}

	public void gravar() {
		PessoaDao daoPessoa = new PessoaDao();
		daoPessoa.adiciona(pessoa);
		pessoas.add(pessoa);
		pessoa = new Pessoa();
	}

	public void remover() {
		PessoaDao daoPessoa = new PessoaDao();
		daoPessoa.remove(pessoa.getCpf());
		pessoas = daoPessoa.listaTodos();
		pessoa = new Pessoa();
	}

	public Pessoa recupera() {
		PessoaDao daoPessoa = new PessoaDao();
		Pessoa pessoaEncontrada = daoPessoa.buscaPorCPF(pessoa.getCpf());
		return pessoaEncontrada;
	}

	public void findAll() {
		PessoaDao daoPessoa = new PessoaDao();
		pessoas = daoPessoa.listaTodos();
	}

	public void atualizar() {
		PessoaDao daoPeessoa = new PessoaDao();
		daoPeessoa.atualiza(pessoa);
		pessoa = new Pessoa();
		pessoas = daoPeessoa.listaTodos();
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public List<Pessoa> getListaDePessoas() {
		return pessoas;
	}
	
	public List<Pessoa> getListaPessoasEncontradas(){
		return pessoasEncontradas;
	}

}
