package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import dao.EmpresaDao;
import model.Empresa;
import model.Organizadora;
import model.Patrocinadora;

@ManagedBean
@ApplicationScoped
public class EmpresaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Patrocinadora patrocinadora = new Patrocinadora();
	private Organizadora organizadora = new Organizadora();

	private List<Empresa> empresasOrganizadoras = new ArrayList<Empresa>();
	private List<Empresa> empresasOrganizadorasEncontrados = new ArrayList<Empresa>();

	private List<Empresa> empresasPatrocinadoras = new ArrayList<Empresa>();
	private List<Empresa> empresasPatrocinadorasEncontrados = new ArrayList<Empresa>();

	private List<Empresa> todasAsEmpresas = new ArrayList<Empresa>();

	public EmpresaBean() {
		this.findAll();
	}

	public void findAll() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		todasAsEmpresas = daoEmpresa.listaTodos(""); 
	}

	public List<Empresa> getListaDeEmpresas() {
		return todasAsEmpresas;
	}

	public void gravarPatrocinadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		daoEmpresa.adiciona(patrocinadora);
		empresasPatrocinadoras.add(patrocinadora);
		findAll();
		patrocinadora = new Patrocinadora();
	}

	public void gravarOrganizadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		daoEmpresa.adiciona(organizadora);
		empresasOrganizadoras.add(organizadora);
		findAll();
		organizadora = new Organizadora();
	}

	public void removerPatrocinadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		daoEmpresa.remove(patrocinadora.getCnpj());
		empresasPatrocinadoras = daoEmpresa.listaTodos(patrocinadora.getClass().toString());
		findAll();
		patrocinadora = new Patrocinadora();
	}

	public void removerOrganizadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		daoEmpresa.remove(organizadora.getCnpj());
		empresasOrganizadoras = daoEmpresa.listaTodos(organizadora.getClass().toString());
		findAll();
		organizadora = new Organizadora();
	}

	public Empresa recuperaPatrocinadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		Empresa pessoaEncontrada = daoEmpresa.buscaPorCNPJ(patrocinadora.getCnpj(),
				patrocinadora.getClass().toString());
		return pessoaEncontrada;
	}

	public Empresa recuperaOrganizadora() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		Empresa pessoaEncontrada = daoEmpresa.buscaPorCNPJ(organizadora.getCnpj(), organizadora.getClass().toString());
		return pessoaEncontrada;
	}

	public void findAllOrganizadoras() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		empresasOrganizadoras = daoEmpresa.listaTodos(organizadora.getClass().toString());
	}

	public void findAllPatrocinadoras() {
		EmpresaDao daoEmpresa = new EmpresaDao();
		empresasPatrocinadoras = daoEmpresa.listaTodos(patrocinadora.getClass().toString());
	}

	public void atualizarPatrocinadora() {
		EmpresaDao daoPeessoa = new EmpresaDao();
		daoPeessoa.atualiza(patrocinadora);
		patrocinadora = new Patrocinadora();
		empresasPatrocinadoras = daoPeessoa.listaTodos(patrocinadora.getClass().toString());
	}

	public void atualizarOrganizadora() {
		EmpresaDao daoPeessoa = new EmpresaDao();
		daoPeessoa.atualiza(organizadora);
		organizadora = new Organizadora();
		empresasOrganizadoras = daoPeessoa.listaTodos(organizadora.getClass().toString());
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Patrocinadora getPatrocinadora() {
		return patrocinadora;
	}

	public Organizadora getOrganizadora() {
		return organizadora;
	}

	public List<Empresa> getEmpresasOrganizadoras() {
		return empresasOrganizadoras;
	}

	public void setEmpresasOrganizadoras(List<Empresa> empresasOrganizadoras) {
		this.empresasOrganizadoras = empresasOrganizadoras;
	}

	public List<Empresa> getEmpresasOrganizadorasEncontrados() {
		return empresasOrganizadorasEncontrados;
	}

	public void setEmpresasOrganizadorasEncontrados(List<Empresa> empresasOrganizadorasEncontrados) {
		this.empresasOrganizadorasEncontrados = empresasOrganizadorasEncontrados;
	}

	public List<Empresa> getEmpresasPatrocinadoras() {
		return empresasPatrocinadoras;
	}

	public void setEmpresasPatrocinadoras(List<Empresa> empresasPatrocinadoras) {
		this.empresasPatrocinadoras = empresasPatrocinadoras;
	}

	public List<Empresa> getEmpresasPatrocinadorasEncontrados() {
		return empresasPatrocinadorasEncontrados;
	}

}