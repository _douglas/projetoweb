package controller;

import dao.CategoriaDao;
import model.Categoria;

public class CategoriaBean {
	
	private Categoria categoria = new Categoria();
	
	public String create(){
		CategoriaDao daoCategoria = new CategoriaDao();
		daoCategoria.adiciona(categoria);
		return "categoria";
	}
	
	public String atualiza(){
		CategoriaDao daoCategoria = new CategoriaDao();
		daoCategoria.atualiza(categoria);
		return "categoria";
	}
	
	public String remove(){
		CategoriaDao daoCategoria = new CategoriaDao();
		daoCategoria.remove(categoria.getId());
		return "categoria";
	}

}
