package controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import dao.LocalDao;
import model.Local;

@ManagedBean
public class LocalBean {

	private long id;

	protected List<Local> locais = new ArrayList<Local>();

	public List<Local> getListaDeLocais() {
		LocalDao daoLocal = new LocalDao();
		locais = daoLocal.listaTodos();
		return locais;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String remove(){
		LocalDao daoLocal = new LocalDao();
		daoLocal.remove(id);
		return "exibirLocais";
	}
	
}
