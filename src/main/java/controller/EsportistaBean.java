package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;

import dao.DAO;
import dao.JPAUtil;
import dao.EsportistaDao;
import model.Esportista;
import model.Pessoa;


@ManagedBean
@ApplicationScoped
public class EsportistaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Esportista esportista = new Esportista();
	public List<Esportista> esportistas = new ArrayList<Esportista>();
	public List<Esportista> esportistasEncontrados = new ArrayList<Esportista>();

	public EsportistaBean() {
		this.findAll();
	}

	public void gravar() {
		EsportistaDao daoEsportista = new EsportistaDao();
		daoEsportista.adiciona(esportista);
		esportistas.add(esportista);
		esportista = new Esportista();
	}

	public void remover() {
		EsportistaDao daoEsportista = new EsportistaDao();
		daoEsportista.remove(esportista.getCpf());
		esportistas = daoEsportista.listaTodos();
		esportista = new Esportista();
	}

	public Esportista recupera() {
		EsportistaDao daoEsportista = new EsportistaDao();
		Esportista pessoaEncontrada = daoEsportista.buscaPorCPF(esportista.getCpf());
		return pessoaEncontrada;
	}

	public void findAll() {
		EsportistaDao daoEsportista = new EsportistaDao();
		esportistas = daoEsportista.listaTodos();
	}

	public void atualizar() {
		EsportistaDao daoPeessoa = new EsportistaDao();
		daoPeessoa.atualiza(esportista);
		esportista = new Esportista();
		esportistas = daoPeessoa.listaTodos();
	}

	public Esportista getEsportista() {
		return esportista;
	}

	public List<Esportista> getListaDeEsportistas() {
		return esportistas;
	}
	
	public List<Esportista> getListaEsportistasEncontrados(){
		return esportistasEncontrados;
	}

}
