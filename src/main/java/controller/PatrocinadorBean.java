package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;

import dao.JPAUtil;
import dao.PatrocinadorDao;
import model.Patrocinador;

@ManagedBean
@ApplicationScoped
public class PatrocinadorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Patrocinador patrocinador = new Patrocinador();
	public List<Patrocinador> patrocinadores = new ArrayList<Patrocinador>();
	public List<Patrocinador> patrocinadorsEncontradas = new ArrayList<Patrocinador>();

	public PatrocinadorBean() {
		this.findAll();
	}

	public void gravar() {
		PatrocinadorDao daoPatrocinador = new PatrocinadorDao();
		daoPatrocinador.adiciona(patrocinador);
		patrocinadores.add(patrocinador);
		patrocinador = new Patrocinador();
	}

	public void remover() {
		PatrocinadorDao daoPatrocinador = new PatrocinadorDao();
		daoPatrocinador.remove(patrocinador.getCpf());
		patrocinadores = daoPatrocinador.listaTodos();
		patrocinador = new Patrocinador();
	}

	public Patrocinador recupera() {
		PatrocinadorDao daoPatrocinador = new PatrocinadorDao();
		Patrocinador patrocinadorEncontrada = daoPatrocinador.buscaPorCPF(patrocinador.getCpf());
		return patrocinadorEncontrada;
	}

	public void findAll() {
		PatrocinadorDao daoPatrocinador = new PatrocinadorDao();
		patrocinadores = daoPatrocinador.listaTodos();
	}

	public void atualizar() {
		PatrocinadorDao daoPeessoa = new PatrocinadorDao();
		daoPeessoa.atualiza(patrocinador);
		patrocinador = new Patrocinador();
		patrocinadores = daoPeessoa.listaTodos();
	}

	public Patrocinador getPatrocinador() {
		return patrocinador;
	}

	public List<Patrocinador> getListaDePatrocinadores() {
		return patrocinadores;
	}
	
	public List<Patrocinador> getListaPatrocinadoresEncontradas(){
		return patrocinadorsEncontradas;
	}

}
