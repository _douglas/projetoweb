package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;

import dao.DAO;
import dao.EmpresaDao;
import dao.JPAUtil;
import dao.OrganizadorDao;
import model.Organizador;
import model.Organizadora;

@ManagedBean
@ApplicationScoped
public class OrganizadorBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Organizador organizador = new Organizador();
	public List<Organizador> pessoas = new ArrayList<Organizador>();
	public List<Organizador> pessoasEncontradas = new ArrayList<Organizador>();

	public OrganizadorBean() {
		this.findAll();
	}

	public String gravar() {
		OrganizadorDao daoOrganizador = new OrganizadorDao();
		EmpresaDao daoEmpresa = new EmpresaDao();
		daoOrganizador.adiciona(organizador);
		pessoas.add(organizador);
		organizador = new Organizador();

		return "organizador";
	}

	public void remover() {
		OrganizadorDao daoOrganizador = new OrganizadorDao();
		daoOrganizador.remove(organizador.getCpf());
		pessoas = daoOrganizador.listaTodos();
		organizador = new Organizador();
	}

	public Organizador recupera() {
		OrganizadorDao daoOrganizador = new OrganizadorDao();
		Organizador pessoaEncontrada = daoOrganizador.buscaPorCPF(organizador.getCpf());
		return pessoaEncontrada;
	}

	public void findAll() {
		OrganizadorDao daoOrganizador = new OrganizadorDao();
		pessoas = daoOrganizador.listaTodos();
	}

	public void atualizar() {
		OrganizadorDao daoOrganizador = new OrganizadorDao();
		daoOrganizador.atualiza(organizador);
		organizador = new Organizador();
		pessoas = daoOrganizador.listaTodos();
	}

	public Organizador getOrganizador() {
		return organizador;
	}

	public List<Organizador> getListaDeOrganizadors() {
		return pessoas;
	}

	public List<Organizador> getListaOrganizadorsEncontradas() {
		return pessoasEncontradas;
	}

}
