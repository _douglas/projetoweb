package controller;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.text.SimpleDateFormat;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

import dao.LocalDao;
import model.HorarioReserva;
import model.LocalPrivado;

@ManagedBean
public class LocalPrivadoBean extends LocalBean {

	private LocalPrivado localPrivado = new LocalPrivado();
	private HorarioReserva horarioReservar = new HorarioReserva();

	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }

	public LocalPrivado getLocalPrivado() {
		return localPrivado;
	}

	public HorarioReserva getHorarioReservar() {
		return horarioReservar;
	}
	
	public String reservaLocalPrivado(){
		localPrivado.getHorariosAlugados().add(horarioReservar);
		LocalDao daoLocal = new LocalDao();
		daoLocal.adiciona(localPrivado);
		return "exibirLocais";
	}
	
	public String atualizaLocal(){
		LocalDao daoLocal = new LocalDao();
		daoLocal.atualiza(localPrivado);
		return "exibirLocais";
	}
	
}
