package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Patrocinador;

public class PatrocinadorDao {

	public void adiciona(Patrocinador patrocinador) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(patrocinador);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Patrocinador patrocinadorARemover = (Patrocinador) em.createQuery("select p from Patrocinador p where p.cpf='" + cpf + "'")
				.getSingleResult();
		em.remove(patrocinadorARemover);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Patrocinador patrocinador) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Patrocinador where cpf='" + patrocinador.getCpf() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(patrocinador);

		em.getTransaction().commit();
		em.close();

	}

	public List<Patrocinador> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Patrocinador";
		em.getTransaction().begin();
		List<Patrocinador> lista = (List<Patrocinador>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Patrocinador buscaPorCPF(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		Patrocinador patrocinador = (Patrocinador) em.createQuery("Select p from Patrocinador p where p.cpf='" + cpf + "'").getSingleResult();
		em.close();
		return patrocinador;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from patrocinador n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Patrocinador> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Patrocinador> query = em.getCriteriaBuilder().createQuery(Patrocinador.class);
		query.select(query.from(Patrocinador.class));

		List<Patrocinador> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}
}
