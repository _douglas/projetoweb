package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Local;

public class LocalDao {

	public void adiciona(Local local) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(local);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(long id) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Local localr = (Local) em.createQuery("select p from Local p where p.id='" + id + "'")
				.getSingleResult();
		em.remove(localr);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Local local) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Local where id='" + local.getId() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(local);

		em.getTransaction().commit();
		em.close();

	}

	public List<Local> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Local";
		em.getTransaction().begin();
		List<Local> lista = (List<Local>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Local buscaPorID(long id) {
		EntityManager em = new JPAUtil().getEntityManager();
		Local local = (Local) em.createQuery("Select p from Local p where p.id='" + id + "'").getSingleResult();
		em.close();
		return local;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from local n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Local> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Local> query = em.getCriteriaBuilder().createQuery(Local.class);
		query.select(query.from(Local.class));

		List<Local> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}

}
