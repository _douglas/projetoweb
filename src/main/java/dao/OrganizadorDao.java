package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Esportista;
import model.Organizador;

public class OrganizadorDao {

	public void adiciona(Organizador organizador) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(organizador);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Organizador pessoaARemover = (Organizador) em.createQuery("select p from Organizador p where p.cpf='" + cpf + "'")
				.getSingleResult();
		em.remove(pessoaARemover);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Organizador organizador) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Organizador where cpf='" + organizador.getCpf() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(organizador);

		em.getTransaction().commit();
		em.close();

	}

	public List<Organizador> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Organizador";
		em.getTransaction().begin();
		List<Organizador> lista = (List<Organizador>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Organizador buscaPorCPF(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		Organizador organizador = (Organizador) em.createQuery("Select p from Organizador p where p.cpf='" + cpf + "'").getSingleResult();
		em.close();
		return organizador;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from organizador n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Organizador> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Organizador> query = em.getCriteriaBuilder().createQuery(Organizador.class);
		query.select(query.from(Organizador.class));

		List<Organizador> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}

}
