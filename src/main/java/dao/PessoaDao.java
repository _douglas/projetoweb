package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Pessoa;

public class PessoaDao {

	public void adiciona(Pessoa pessoa) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(pessoa);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Pessoa pessoaARemover = (Pessoa) em.createQuery("select p from Pessoa p where p.cpf='" + cpf + "'")
				.getSingleResult();
		em.remove(pessoaARemover);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Pessoa pessoa) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Pessoa where cpf='" + pessoa.getCpf() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(pessoa);

		em.getTransaction().commit();
		em.close();

	}

	public List<Pessoa> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Pessoa";
		em.getTransaction().begin();
		List<Pessoa> lista = (List<Pessoa>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Pessoa buscaPorCPF(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		Pessoa pessoa = (Pessoa) em.createQuery("Select p from Pessoa p where p.cpf='" + cpf + "'").getSingleResult();
		em.close();
		return pessoa;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from livro n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Pessoa> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Pessoa> query = em.getCriteriaBuilder().createQuery(Pessoa.class);
		query.select(query.from(Pessoa.class));

		List<Pessoa> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}
}
