package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Empresa;
import model.Organizadora;

public class EmpresaDao {

	public void adiciona(Empresa empresa) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(empresa);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(String cnpj) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Empresa pessoaARemover = (Empresa) em.createQuery("select p from Empresa p where p.cnpj='" + cnpj + "'")
				.getSingleResult();
		em.remove(pessoaARemover);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Empresa empresa) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Empresa where cnpj='" + empresa.getCnpj() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(empresa);

		em.getTransaction().commit();
		em.close();

	}

	public List<Empresa> listaTodos(String tipoDaEmpresa) {
		EntityManager em = new JPAUtil().getEntityManager();
		String query;
		if (tipoDaEmpresa.equalsIgnoreCase("")) {
			query = "from Empresa";
		} else {
			query = "from Empresa where dtype='" + tipoDaEmpresa + "'";
		}
		em.getTransaction().begin();
		List<Empresa> lista = (List<Empresa>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Empresa buscaPorCNPJ(String cnpj, String tipoDaEmpresa) {
		EntityManager em = new JPAUtil().getEntityManager();
		Empresa empresa = (Empresa) em
				.createQuery("Select p from Empresa p where p.cnpj='" + cnpj + "' and p.dtype='" + tipoDaEmpresa + "'")
				.getSingleResult();
		em.close();
		return empresa;
	}

	public int contaTodos(String tipoDaEmpresa) {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from Empresa n where dtype='" + tipoDaEmpresa + "'")
				.getSingleResult();
		em.close();

		return (int) result;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from Empresa n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Empresa> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Empresa> query = em.getCriteriaBuilder().createQuery(Empresa.class);
		query.select(query.from(Empresa.class));

		List<Empresa> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}
	
}
