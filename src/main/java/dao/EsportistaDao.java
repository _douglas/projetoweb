package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Esportista;
import model.Pessoa;

public class EsportistaDao {

	public void adiciona(Esportista esportista) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(esportista);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Esportista pessoaARemover = (Esportista) em.createQuery("select p from Pessoa p where p.cpf='" + cpf + "' and dtype='Esportista'")
				.getSingleResult();
		em.remove(pessoaARemover);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Esportista esportista) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Pessoa where cpf='" + esportista.getCpf() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(esportista);

		em.getTransaction().commit();
		em.close();

	}

	public List<Esportista> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Pessoa where dtype='Esportista'";
		em.getTransaction().begin();
		List<Esportista> lista = (List<Esportista>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Esportista buscaPorCPF(String cpf) {
		EntityManager em = new JPAUtil().getEntityManager();
		Esportista esportista = (Esportista) em.createQuery("Select p from Pessoa p where p.cpf='" + cpf + "' and dtype='Esportista'").getSingleResult();
		em.close();
		return esportista;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from Pessoa n and dtype='Esportista'").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Esportista> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Esportista> query = em.getCriteriaBuilder().createQuery(Esportista.class);
		query.select(query.from(Esportista.class));

		List<Esportista> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}
}
