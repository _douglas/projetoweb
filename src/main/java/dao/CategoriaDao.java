package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import model.Categoria;

public class CategoriaDao {

	public void adiciona(Categoria categoria) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();

		// abre transacao
		em.getTransaction().begin();

		// persiste o objeto
		em.persist(categoria);

		// commita a transacao
		em.getTransaction().commit();

		// fecha a entity manager
		em.close();
	}

	public void remove(long id) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		Categoria categoriar = (Categoria) em.createQuery("select p from Categoria p where p.id='" + id + "'")
				.getSingleResult();
		em.remove(categoriar);
		em.getTransaction().commit();
		em.close();
	}

	public void atualiza(Categoria categoria) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		String queryDelete = "delete from Categoria where id='" + categoria.getId() + "'";
		em.createQuery(queryDelete).executeUpdate();
		em.persist(categoria);

		em.getTransaction().commit();
		em.close();

	}

	public List<Categoria> listaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		String query = "from Categoria";
		em.getTransaction().begin();
		List<Categoria> lista = (List<Categoria>) em.createQuery(query).getResultList();
		em.getTransaction().commit();
		em.close();
		return lista;
	}

	public Categoria buscaPorID(long id) {
		EntityManager em = new JPAUtil().getEntityManager();
		Categoria categoria = (Categoria) em.createQuery("Select p from Categoria p where p.id='" + id + "'").getSingleResult();
		em.close();
		return categoria;
	}

	public int contaTodos() {
		EntityManager em = new JPAUtil().getEntityManager();
		long result = (Long) em.createQuery("select count(n) from categoria n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<Categoria> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<Categoria> query = em.getCriteriaBuilder().createQuery(Categoria.class);
		query.select(query.from(Categoria.class));

		List<Categoria> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults)
				.getResultList();

		em.close();
		return lista;
	}

}
