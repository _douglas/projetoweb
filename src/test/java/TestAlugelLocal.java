import model.Categoria;
import model.Evento;
import model.LocalPrivado;
import model.LocalPublico;
import model.Organizador;
import model.Organizadora;
import model.Patrocinador;
import model.Patrocinadora;

public class TestAlugelLocal {
	public static void main(String args[]){
		
		//Criar Local P�blico e Privado
		LocalPrivado lprivado = new LocalPrivado();
		LocalPublico lpublico = new LocalPublico();
		
		lprivado.setBairro("bom fim");
		lprivado.setCep("05588995");
		lprivado.setNumero("225");
		lprivado.setRua("ediviges");
		lprivado.setValorAluguel(225.34);
		
		
		lpublico.setBairro("escola ppg");
		lpublico.setCep("05585545");
		lpublico.setNumero("223");
		lpublico.setRua("anuncia��o");
		
		//Criar Categoria
		Categoria categoria = new Categoria();
		categoria.setNome("futebol");
		
		//Criar Organizadores
		Organizador org1 = new Organizador();
		Organizadora org2 = new Organizadora();		
		Organizador org3 = new Organizador();
		
		org1.setCpf("132321");
		org1.setHorasDedicadas(31);
		org1.setNome("Francisco");
		
		org3.setCpf("646222");
		org3.setHorasDedicadas(31);
		org3.setNome("Ferdinando");
		
		org2.setCnpj("98798797");
		org2.setEndereco("Avenida Princesa Paula");
		org2.setHorasDedicadasDostrabalhadores();
		
		org2.getOrganizadores().add(org1);
		org2.getOrganizadores().add(org3);
		
		Patrocinador pat1 = new Patrocinador();
		Patrocinadora pat2 = new Patrocinadora();
		
		pat1.setCpf("098098");
		pat1.setNome("joana");
		pat1.setVerbaAInvestir(8855);
		
		pat2.setCnpj("87987987");
		pat2.setEndereco("lakdjfalkdjfalkjdf");
		pat2.setRazaoSocial("firma minha vida");
		pat2.setVerbaAInvestir(987989);
		
		Evento evento = new Evento();
		evento.setCategoria(categoria);
		evento.setNome("Campeonato Infantil");
		evento.setOrganizadores(org2.getOrganizadores());
		evento.getPatrocinadores().add(pat1);
		evento.setVerbaDispoonivel(pat1.getVerbaAInvestir() + pat2.getVerbaAInvestir());
		
		System.out.println("Ok");

		
	}
}
